<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
/*
Route::get('/', function () {
    return view('welcome');
});*/
Route::get('/', 'JobController@overview')->name('overview');
Route::get('/{category}/{state}', 'JobController@publish')->name('group');
Route::get('/{category}/{state}/{job}', 'JobController@publish')->name('job');

