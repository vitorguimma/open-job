<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class JobController extends Controller
{
	private $path_view = 'screen.job';
    
    public function publish($category,$state=null,$job=null){
    	
    	$args = array(
    		'title_page'=>'Vagas - '.$category,
    		'category'=>$category,
    		'state'=>$state,
    		'location'=>array('SP','RJ','SC','BH'),
    		'jobs'=>array(
	    		'2332'=>array(
					'title'=>'Ator',
					'description'=>'dasdasd as asdasd asd as asd',
					'state'=>'SP',
					'city'=>'São Paulo',
					'office_hour'=>'Seg a Qua das 10:00 as 16:00',
					'profile'=>'Profissional',
					'hierarquical_level'=>'Especialista',
					'contract_type'=>'PJ',
					'Payment_periode'=>'Mes',
					'salary_as'=>'v',//c = Combinar, f = fechad, v = variavel
					'salary'=>'R$ 8.000,00',
					'salary_min'=>'R$ 0,00',
					'salary_max'=>'R$ 8.000,00',
					'created_at'=>'22/10/2016',
					'publish_at'=>'30/12/2016',
					'expires_at'=>'31/12/2016',

					'show_company_name'=>'S',
					'show_company_detail'=>'S',
					 'show_speak_to'=>'S',

					 'company_name'=>'Globo',
					 'company_detail'=>'dasdsadsad asdsadas dsa',

					 'contact'=>array(
					 		array(
					    		'tipe'=>'Telefone',
					    		'value'=>'11 4489-5948',
					    		'speak_to'=>'Fernanda'
					    	),
					    	array(
					    		'tipe'=>'Telefone',
					    		'value'=>'11 4489-5948',
					    		'speak_to'=>'Fernanda'
					    	),
					  ),
					    
					'require'=>array(
							'academic'=>array(
						    		array(
						    			'type'=>'bacharel',
						    			'title'=>'Marketing',
						    			'completed'=>'S',
						    		),
						    		array(
						    			'type'=>'bacharel',
						    			'title'=>'Marketing',
						    			'completed'=>'S',
						    		),
						    ),
						    'skills'=>array(
						    				array(
						    					'title'=>'Framework Laravel',
						    					'time_at_last'=>'4 anos'
						    				),
						    				array(
						    					'title'=>'Framework Laravel',
						    					'time_at_last'=>'4 anos'
						    				),
						    ),
						   'certification'=>array(
						    	'MCSA',
						    	'TOFEL'
						    ),
						    'languages'=>array(
						    				array(
						    					'title'=>'Ingles',
						    					'level'=>'Fluente'
						    				),
						    				array(
						    					'title'=>'Ingles',
						    					'level'=>'Fluente'
						    				),
						    ),
						    'plus'=>array(
						    	'Conhecimento em Medicina',
						    	'Ter trabalhado alguns anos em Tecnologia'
						    )
					 )//Requerido	
				),
				'2333'=>array(
					'title'=>'Aux. Administrativo',
					'description'=>'dasdasd as asdasd asd as asd',
					'state'=>'SP',
					'city'=>'Campinas',
					'office_hour'=>'Seg a Qua das 10:00 as 16:00',
					'profile'=>'Profissional',
					'hierarquical_level'=>'Especialista',
					'contract_type'=>'CLT',
					'Payment_periode'=>'Mes',
					'salary_as'=>'Combinar',//Combinar, Salario, De xxx a xxx
					'salary'=>'R$ 8.000,00',
					'salary_min'=>'R$ 0,00',
					'salary_max'=>'R$ 8.000,00',
					'created_at'=>'22/10/2016',
					'publish_at'=>'30/12/2016',
					'expires_at'=>'31/12/2016',

					'show_company_name'=>'S',
					'show_company_detail'=>'S',
					 'show_speak_to'=>'N',

					 'company_name'=>'Google',
					 'company_detail'=>'dasdsadsad asdsadas dsa',

					 'contact'=>array(
					 		array(
					    		'tipe'=>'Telefone',
					    		'value'=>'11 4489-5948',
					    		'speak_to'=>'Fernanda'
					    	),
					    	array(
					    		'tipe'=>'Telefone',
					    		'value'=>'11 4489-5948',
					    		'speak_to'=>'Fernanda'
					    	),
					  ),
					    
					'require'=>array(
							'academic'=>array(
						    		array(
						    			'type'=>'Bacharel',
						    			'title'=>'Marketing',
						    			'completed'=>'S',
						    		),
						    		array(
						    			'type'=>'Mestrado',
						    			'title'=>'Marketing',
						    			'completed'=>'N',
						    		),
						    ),
						    'skills'=>array(
						    				array(
						    					'title'=>'Framework Laravel',
						    					'time_at_last'=>'4 anos'
						    				),
						    				array(
						    					'title'=>'Framework Laravel',
						    					'time_at_last'=>''
						    				),
						    ),
						   'certification'=>array(
						    	'MCSA',
						    	'TOFEL'
						    ),
						    'languages'=>array(
						    				array(
						    					'title'=>'Ingles',
						    					'level'=>'Fluente'
						    				),
						    				array(
						    					'title'=>'Ingles',
						    					'level'=>'Fluente'
						    				),
						    ),
						    'plus'=>array(
						    	'Conhecimento em Medicina',
						    	'Ter trabalhado alguns anos em Tecnologia'
						    )
					 )//Requerido	
				),
			)	    					
    	);
		if($job){
			$reg = $args['jobs'][$job];
			$reg['title_page']=$args['title_page'];
			$reg['category']=$args['category'];
			$reg['state']=$args['state'];
			
			return view("$this->path_view.job",$reg);
		}else{
			return view("$this->path_view.group",$args);
		}
	}

     public function overview(){
    	$args = array(
    			'title_page'=>'Visão Geral',
    			'jobs'=>array(
    				'Administração'=>array(
    					'location'=>array('SP','RJ','SC'),
    					'total'=>3500,
    					'companies'=>200
    				),
    				'Tecnologia'=>array(
    					'location'=>array('SP','RJ','SC'),
    					'total'=>3500,
    					'companies'=>200
    				),
    				'Arquitetura Artes Design'=>array(
    					'location'=>array('SP','RJ','SC'),
    					'total'=>3500,
    					'companies'=>200
    				),
    			)
    		);
    	return view("$this->path_view.overview",$args);
    }


}
