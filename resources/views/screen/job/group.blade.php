<link rel="stylesheet" href="{{ URL::asset('css/default.css') }}">
<title>{{$title_page}}</title>
 <div class="block-container">
    <div class="block-title">
        <ul class="blobk-bradcumb">
          <li><a href="{{route('overview')}}">Visão Geral</a></li>
          <li><a href="{{route('group',['category'=>$category,'state'=>$state])}}"> > {{$category}}</a></li>
        </ul>
    </div>
    <div class="block-menu">
      <a href="{{route('group',['category'=>$category,'state'=>'geral'])}}" class="shadow0 @if('geral'==$state) block-active @endif">Todos</a>
      @foreach($location as $loc)
        <a href="{{route('group',['category'=>$category,'state'=>$loc])}}" class="shadow0 @if($loc==$state) block-active @endif">{{$loc}}</a>
      @endforeach
    </div>
    @foreach ($jobs as $unid => $job)
     <a href="{{route('job',['category'=>$category,'state'=>$state,'job'=>$unid])}}">
       <div class="block-category shadow0">
          <div class="block-header">           
            <p>{{$job['title']}}</p>
          </div>
          <div class="block-content">
            <div class="block-detail">
              @if($job['salary_as']==='c')
                <p>A Combinar</p>
              @elseif($job['salary_as']==='v')
                <p>{{$job['salary_min']}} a {{$job['salary_max']}}</p>
              @else
                <p>{{$job['salary']}}</p>
              @endif

              
              <p><b>UF: </b>{{$job['state']}}</p>
              <p><b>Cidade: </b>{{$job['city']}}</p>
              <p><b>Vinculo: </b>{{$job['contract_type']}}</p>
              <p><b>Hierarquia:</b> {{$job['hierarquical_level']}}</p>
            </div>
          </div>
       </div>
     </a>
    @endforeach
</div>
