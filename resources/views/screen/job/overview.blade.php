<link rel="stylesheet" href="{{ URL::asset('css/default.css') }}">
  <title>{{$title_page}}</title>
 <div class="block-container">
    <div class="block-title shadow0">
        <ul class="blobk-bradcumb">
          <li><a href="{{route('overview')}}">Visão Geral</a></li>
        </ul>
    </div>
    @foreach ($jobs as $category => $detail)
     <a href="{{route('group',['categoria'=>$category,'state'=>'geral'])}}">
       <div class="block-category shadow0">
          <div class="block-header">           
            <p>{{$category}}</p>
          </div>
          <div class="block-content">
            <p>{{implode(', ',$detail['location'])}}</p>  
            <p><b>{{$detail['total']}}</b> Propostas</p>  
            <p><b>{{$detail['companies']}}</b> Empresas</p>  
          </div>
       </div>
     </a>
    @endforeach
</div>
