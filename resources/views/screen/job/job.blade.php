<link rel="stylesheet" href="{{ URL::asset('css/default.css') }}">
<title>{{$title_page}}</title>
 <div class="block-container">
    <div class="block-title">
       <ul class="blobk-bradcumb">
          <li><a href="{{route('overview')}}">Visão Geral</a></li>
          <li><a href="{{route('group',['category'=>$category,'state'=>$state])}}"> > {{$category}}</a></li>
          <li> > {{$title}}</li>
        </ul>
    </div>
    <div class="block-record shadow0">
          <div class="block-header">           
            <p>{{$title}}</p>
          </div>
          <div class="block-record-content">
              <div class="block-section">
                  <p>{{$company_name}}</p>
                  <p>{{$city}} - {{$state}}</p>  
                  
              </div>    

              <div class="block-section">
                  <p>Sobre a Empresa</p>  
                 <p>{{$company_detail}}</p>   
              </div>
              <div class="block-section">
                  <p>Sobre a Vaga</p>  
                 <p>{{$description}}</p> 
              </div>  
              <div class="block-section">
                  <p>Remuneração</p>  
                 @if($salary_as==='c')
                    <p>A Combinar</p>
                  @elseif($salary_as==='v')
                    <p>{{$salary_min}} a {{$salary_max}}</p>
                  @else
                    <p>{{$salary}}</p>
                  @endif
              </div>  
              <div class="block-section">
                  <p>Requisitos</p> 
                  <p>Ser Formado na(s) Area(s)</p> 
                  <ul>
                  @foreach($require['academic'] as $item)
                     <li>{{$item['type']}} @if($item['completed']=='S') comcluído @endif em {{$item['title']}} </li>
                  @endforeach
                  </ul>

                  <p>Possuir Habilidade(s) com</p> 
                  <ul>
                  @foreach($require['skills'] as $item)
                     <li>{{$item['title']}} @if(!empty($item['time_at_last']))(Mínimo {{$item['time_at_last']}} de experiência) @endif</li>
                  @endforeach
                  </ul>

                  <p>Possuir a(s) Certificações</p> 
                  <ul>
                  @foreach($require['certification'] as $item)
                     <li>{{$item}}</li>
                  @endforeach
                  </ul>

                  <p>Falar o(s) Idioma(s)</p> 
                  <ul>
                  @foreach($require['languages'] as $item)
                     <li>{{$item['title']}} - {{$item['level']}}</li>
                  @endforeach
                  </ul>

                  <p>Consideramos Diferencial</p> 
                  <ul>
                  @foreach($require['plus'] as $item)
                     <li>{{$item}}</li>
                  @endforeach
                  </ul>
                 
              </div> 

               <div class="block-section">
                  <p>Contato</p> 
                  <ul>
                  @foreach($contact as $item)
                     <li>{{$item['value']}} @if($show_speak_to=='S') - {{$item['speak_to']}} @endif</li>
                  @endforeach
                  </ul>

              </div>
          </div>
       </div>
     
</div>
